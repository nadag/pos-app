const app=require('./config/expressCon');
const dbCon=require('./config/dbCon');
var PORT=8000||process.env.PORT;

app.listen(PORT, () => {
    console.log(`Server is listening to the port ${PORT}`);
  });
      
module.exports=app;
