const appRouter=require('express').Router();
const appControllers=require('./controllers');


//routes for cashiers


//get all cashiers
appRouter.route('/api/cashiers').get(function(req,res){
    appControllers.getAllCashiers(req,res);
})


// cashier auth by email
appRouter.route('/api/login').post(function(req,res){
    appControllers.cashierLogin(req,res);
})

// add a new cashier
appRouter.route('/api/newcashier').post(function(req,res){
    appControllers.addNewCashier(req,res);
})


// display data of a selected cashier


// update a given cashier info

// remove a given cashier


//..........................................
// routes for transctions


// handle get all transctions based of the current date



// get all transctions of a given date


// get all transction of a selected cashier


//............................

appRouter.route('/api/storetransction').post(function(req,res){
    appControllers.storeTransction(req,res);
})



appRouter.route('/api/gettranscashier').post(function(req,res){
    appControllers.getTransctions(req,res);
})



// bills routes

appRouter.route('/api/newbill').post(function(req,res){
    appControllers.addNewBill(req,res);
})




module.exports=appRouter;