const dbCon=require('../config/dbCon');

function getAllCashiers(req,res){
    let $cashiers="SELECT *FROM cashiers";
    dbCon.query($cashiers,function(err,rows,fields){
        if(err){
            res.status(404).send({error:err});            
        }
        else{
            res.status(201).send({cashiers:rows}); 
        }
    })
}


function cashierLogin(req,res){
    const cashierEmail=req.body.email;

    // check if the email exists in the DB
    let $query=`SELECT * FROM cashiers WHERE email='${cashierEmail}'`;
    dbCon.query($query,function(err,result,rows){
        if(err){
            res.sataus(404).send({error:err});
        }
        else{
            if(result.length>0){
                res.status(200).send({found:true});
            }
            else{
                res.status(200).send({found:false});
            }    
        }
    })
}

function addNewCashier(req,res){
    let cashierName= req.body.name;
    let cashierEmail=req.body.email;
    let $query=`INSERT INTO cashiers(name,email) VALUES ('${cashierName}','${cashierEmail}')`;

    // let values=[cashierName,cashierEmail];
    dbCon.query($query,function(err,result,fields){
        if(err){
            res.status(404).send({error:err});
            console.log(err);
        }
        else{
            res.status(200).send({insertResult:result.insertId});
        }
    })
}




function deleteCashier(req,res){

}


// transctions methods
function storeTransction(req,res){
    //console.log(req.body);
    let productCost=req.body.cost;
    let paidAmount=req.body.paid;
    let remainAmount=req.body.remain;
    let cashierEmail=req.body.cashierEmail;
    // get the id of the cashier
    getCashierId(cashierEmail,function(cashierId){

        let $query=`INSERT INTO transctions(product_cost,paid_amount,remain_amount,cashier_id) VALUES ('${productCost}','${paidAmount}','${remainAmount}','${cashierId}')`
        dbCon.query($query,function(err,result,rows){
            if(err){
                console.log(err);
                res.status(404).send({err:err});
            }
            else{
                getBills(function(err,bills){
                    if(err){
                        console.log(err);
                     }
                    else{
                       // var change;
                        var i=bills.length;
                        var $query;
                        var id=result.insertId;

                        // insert the deposit into trans_logs table
                        $query=`INSERT INTO trans_logs(type,bill,amount,transdetails_id) VALUES('deposit','${paidAmount}','1','${id}')`;
                        dbCon.query($query,function(err,res,rows){
                            if(err){
                                console.log(err);
                            }
                            else{
                                console.log('inserted successfully');   
                                computeChange(bills,remainAmount,id);
                            
                            }
                       })  
                      }
                })
            }
        })
    });
}

function computeChange(bills,remainAmount,id){
   var changes=getChange(bills,remainAmount);
   let $query;
    for(var i=0;i<changes.length;i++){
      $query=`INSERT INTO trans_logs(type,bill,amount,transdetails_id) VALUES('widthraw','${changes[i].bill}','${changes[i].change}','${id}')`;
      dbCon.query($query,function(err,result,rows){
          if(err){
              console.log(err);
          }
          else{
              console.log("success insertion into table trans_logs");
          }
      })
    }
  }


function getChange(bills,remainAmount){
    let i=bills.length-1;
    let currentBill;
    let bills_amounts=[];
    //console.log(bills[i].category);
    while(remainAmount>0){
         currentBill=bills[i].category;
         if(remainAmount-currentBill>=0){
            change=Math.floor(remainAmount/currentBill);
             //store bill,change in list of objects
             // you can check if there is enough anmount of the currentBill
             bills_amounts.push({bill:currentBill,change:change});
             remainAmount=remainAmount%currentBill;
        }
        i--;
     }
     return bills_amounts;
}


function getCashierId(email,cb){
    let $query=`SELECT id  FROM cashiers WHERE email='${email}'`;
    let id;
    dbCon.query($query,function(err,result,rows){
        if(err){
            console.log(err);
        }
        else{
            id=JSON.parse(JSON.stringify(result))[0].id;
            cb(id);
        }
    })
}


function getCashierIdByEmail(cashierEmail,cb){
    let $query=`SELECT id FROM cashiers WHERE email='${cashierEmail}'`;
    dbCon.query($query,function(err,result,rows){
        if(err){
            console.log(err);
        }
        else{
            cb(result);

        }
    })

}



//get transctions of a given cashier

function getTransctions(req,res){
    var cashierEmail=req.body.email;
      
   getCashierIdByEmail (cashierEmail,function(id){
        id=JSON.parse(JSON.stringify(id))[0].id;
        let $query=`SELECT * FROM transctions WHERE cashier_id='${id}'`;
        dbCon.query($query,function(err,result,rows){
            if(err){
                res.status(404).send({err:err});
            }
            else{
                result=JSON.parse(JSON.stringify(result));
                res.status(200).send({trans:result});
            }
        })
    })
    }

   // let $query='SELECT * FROM'




// bills methods
function getBills(cb){
    let $billsQuery='SELECT category FROM bills';
    let  billList=[];
    let bills=[];
    dbCon.query($billsQuery,function(err,result,rows){
        if(err){
            res.status(404).send({error:err});
        }
        else{
            bills= JSON.parse(JSON.stringify(result));
        }
       for(var i=0;i<bills.length;i++){
           billList[i]=(bills[i]);
       }
       cb(null,billList);
    })
}

function addNewBill(req,res){
    let billCat=req.body.category;
    let billCount=req.body.count;
    let $query=`INSERT INTO bills(category,count) VALUES ('${billCat}','${billCount}')`;
    dbCon.query($query,function(err,result,rows){
        if(err){
            res.status(404).send({error:err});
        }
        else{
            res.status(200).send({insertResult:result.insertId});
        }
    })

}

module.exports={
    getAllCashiers:getAllCashiers,
    addNewCashier:addNewCashier,
    cashierLogin,cashierLogin,
    addNewBill:addNewBill,
    storeTransction:storeTransction,
    getTransctions:getTransctions
}