const express=require('express');
const app=express();
const morgan = require('morgan'); 
const bodyParser = require('body-parser'); 
app.use(express.static(__dirname + '/public'));               
app.use(morgan('dev'));                                         // log every request to the console
app.use(bodyParser.urlencoded({'extended':'true'}));           
app.use(bodyParser.json());                             

// enable cross origin resources
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

const appRouter=require('./../resources/routes');
app.use('/',appRouter);
module.exports=app;
