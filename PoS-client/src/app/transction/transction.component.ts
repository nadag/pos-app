import { Component, OnInit } from '@angular/core';
import { ChangeService } from '../change.service';
import {Router,ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-transction',
  templateUrl: './transction.component.html',
  styleUrls: ['./transction.component.css']
})
export class TransctionComponent implements OnInit {
  cashierE:string;
  constructor(private Change:ChangeService,
    private router:Router, private route:ActivatedRoute) {
   }

  ngOnInit() {
   this.cashierE=this.route.snapshot.params['cashierEmail'];
  }



  calculateChange(event,cost,paid,remain){
    event.preventDefault();
    remain.value='';
    if(parseInt(cost.value)<parseInt(paid.value)){
      remain.value=paid.value-cost.value;
    }
    else{
      alert('paid should be>=the cost');
    }
    //set remain into the input text 
  }

  findChange(event){
    //event.preventDesfault();
    const target=event.target;
    const cost=target.cost.value;
    const paid=target.paid.value;
    const remain=target.remain.value;
    this.Change.sendTransction(cost,paid,remain,this.cashierE);

  }

}
