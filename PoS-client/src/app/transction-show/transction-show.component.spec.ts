import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransctionShowComponent } from './transction-show.component';

describe('TransctionShowComponent', () => {
  let component: TransctionShowComponent;
  let fixture: ComponentFixture<TransctionShowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransctionShowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransctionShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
