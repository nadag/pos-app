import { Component, OnInit } from '@angular/core';
import { CashiersAllService } from '../cashiers-all.service';
import {GetTransCashierService} from '../get-trans-cashier.service';

@Component({
  selector: 'app-transction-show',
  templateUrl: './transction-show.component.html',
  styleUrls: ['./transction-show.component.css']
})
export class TransctionShowComponent implements OnInit {
  cashiers=[];
  trans:any[];
  constructor(private cashierAll:CashiersAllService,private transAll:GetTransCashierService) {   }

  ngOnInit() {
    //call service to get all cashiers
    this.cashierAll.getCashiers();
    this.cashiers=this.cashierAll.cashiers;  
  }

  getTransByCashier(event){
   // this.trans=[];
    this.transAll.getTrans(event.target.value);
    this.trans=this.transAll.transctionsByCashier;
   console.log(this.trans);
  }


}
