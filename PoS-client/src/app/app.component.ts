import { Component } from '@angular/core';
import { CashiersAllService } from './cashiers-all.service';
import { GetTransCashierService } from './get-trans-cashier.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers:[CashiersAllService,GetTransCashierService]
})
export class AppComponent {
  title = 'app';
}
