import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GetTransCashierService {
 transctionsByCashier=[];
  constructor(private http:HttpClient) { }

  getTrans(email){
    this.http.post('/api/gettranscashier',{
      email:email
    }).subscribe((data)=>{
      this.transctionsByCashier=data["trans"];
     // console.log(this.transctionsByCashier);
    })

  }
}
