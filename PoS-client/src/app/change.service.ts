import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ChangeService {

  constructor(private http:HttpClient) { }

  sendTransction(cost,paid,remain,cashE){
    this.http.post('/api/storetransction',{
      cost:cost,
      paid:paid,
      remain:remain,
      cashierEmail:cashE
    }).subscribe((data)=>{
      console.log(data);

    })
  }
}
