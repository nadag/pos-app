import { TestBed, inject } from '@angular/core/testing';

import { CashiersAllService } from './cashiers-all.service';

describe('CashiersAllService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CashiersAllService]
    });
  });

  it('should be created', inject([CashiersAllService], (service: CashiersAllService) => {
    expect(service).toBeTruthy();
  }));
});
