import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http:HttpClient,
      private router:Router) {

       }

  getUserDetails(email){
    return this.http.post('/api/login',{
      email:email
    }).subscribe(data=>{
      if(data["found"]){
      this.router.navigate(['/transtion-entry',{cashierEmail:email}])
      }
      else{
        alert('email not found');
      }
         }  )

  }
}
