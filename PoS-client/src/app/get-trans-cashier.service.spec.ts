import { TestBed, inject } from '@angular/core/testing';

import { GetTransCashierService } from './get-trans-cashier.service';

describe('GetTransCashierService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GetTransCashierService]
    });
  });

  it('should be created', inject([GetTransCashierService], (service: GetTransCashierService) => {
    expect(service).toBeTruthy();
  }));
});
